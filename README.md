# opl-fe
The Open Practice Library is a community-driven repository of practices and tools. These are shared by people currently using them day-to-day for people looking to be inspired with new ideas and experience. This repository houses the front-end for the Open Practice Library website.

## .env file
Create a .env file and point the ```VUE_APP_CMSURL``` variable to a valid url. You can use the .env-example file as a guide.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Run your end-to-end tests
```
npm run test:e2e
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
